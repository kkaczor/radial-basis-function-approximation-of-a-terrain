function f = approximate_terrain(c, a, beta, o)
x_graph = 0:0.3:10;
y_graph = 0:0.3:10;

x_graph_len = length(x_graph);
f = zeros(x_graph_len,x_graph_len);

for i = 1:x_graph_len
    for j = 1:x_graph_len
        f(i,j) = approximate_([x_graph(j) y_graph(i)], c, a, beta);
    end
end
if(mod(o,20)==3)
    surf(x_graph,y_graph,f)
    grid on;
    shading interp
end