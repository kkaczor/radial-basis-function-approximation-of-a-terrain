function result = my_rbf(x,c,beta)

vector_norm = norm(x-c);

%Gaussian RBF                                    
%result = power(exp(-beta*vector_norm),2);

%Multiquadric
result = sqrt(1+(beta*vector_norm)^2);

%Polyharmonic
%result = vector_norm^beta;

%Polynomial
%result = vector_norm^2+beta^2;