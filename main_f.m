clear;
beta = 0.6;
%dataset_size = length(ft);
%number_of_train_samples = floor(0.9*dataset_size);

z = 0.3;
[X,Y] = meshgrid(0:0.1:10,0:10);
Z = sin(z*X) + cos(z*Y);
figure;
surf(X,Y,Z)
grid on;
shading interp
hold on;

for i = 1:10000
    x_test(i) = rand*10;
    y_test(i) = rand*10;
    z_test(i) = sin(z*x_test(i)) + cos(z*y_test(i));
end
xi_test = [x_test; y_test]';

for i = 1:3
    xi_train(i,1) = rand*10;
    xi_train(i,2) = rand*10;
    z_train(i) = sin(z*xi_train(i,1)) + cos(z*xi_train(i,2));
end

for i = 3:100
    
    if(mod(i,20)==3)
        figure;
        plot3(xi_train(:,1),xi_train(:,2),z_train,'ob');
        hold on;
        shading interp
    end
    
    a = calculate_coeffs_a(xi_train,z_train,beta);
    f_approx = approximate_terrain(xi_train,a,beta,i);
    
    [emax(i), emean(i)] = calculate_error(xi_test,xi_train,z_test,a,beta);

    xi_train(i+1,1) = rand*10;
    xi_train(i+1,2) = rand*10;
    z_train(i+1) = sin(z*xi_train(i+1,1)) + cos(z*xi_train(i+1,2));

end
    figure;
    plot(1:100,emax);
    figure;
    plot(1:100,emean);

  disp('emax:');
  disp(emax);
  disp('emean:');
  disp(emean);