function [emax, emean] = calculate_error(x, c, z_test, a, beta)
len_of_test = length(x);
arr_of_approximations = zeros(len_of_test,1);

for i=1:len_of_test
    arr_of_approximations(i) = approximate_(x(i,:), c, a, beta);
end

error = norm(z_test - arr_of_approximations');
%error = abs(z_test - arr_of_approximations');

emax = max(error);

emean = sum(error)/len_of_test;



