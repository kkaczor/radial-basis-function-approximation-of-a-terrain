function a = calculate_coeffs_a(xj, F, beta)
% xj -> set of pairs of coordinates (x,y)
% F -> value of the Z axis for a pair (x,y) == xj
K = length(F);
fi_ij = zeros(K,K);

for i = 1:K
    vector_i = xj(i,:);
    for j =1:K
        vector_j = xj(j,:);
        fi_ij(i,j)=my_rbf(vector_i,vector_j,beta);
    end
end

a = fi_ij\F';
