function f = approximate_(x, c, a, beta)
K = length(c);
f = 0;

for l=1:K
    f = f + approximate_value(c(l,:),[x(1) x(2)],a(l),beta);
end
